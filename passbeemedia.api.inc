<?php

/**
 * @file
 * Passbeemedia Web Push API calls.
 */

/**
 * Core request to Passbeemedia Web Push API.
 */
function passbeemedia_remote_request($remote_data) {
  $auth_creds = '';
  if (!empty($remote_data['appkey'])) {
    $auth_creds = 'Basic ' . base64_encode($remote_data['appkey'] . ':' . $remote_data['appsecret']);
  }
  $remote_host = 'http://webpush.passbeemedia.com/api/';
if($remote_data['remoteAction'] == 'push')
	$remote_url = $remote_host . 'v1/' . $remote_data['remoteAction'];
else
	$remote_url = $remote_host . $remote_data['remoteAction'];

  $headers = array(
    'Authorization'  => $auth_creds,
    'Accept'       => 'application/json',
    'Content-Type'   => 'application/json',
    'Content-Length' => strlen($remote_data['remoteContent']),
  );

  $remote_payload = array(
    'method'    => $remote_data['method'],
    'headers'   => $headers,
    'data'      => $remote_data['remoteContent'],
  );
  $response = drupal_http_request($remote_url, $remote_payload);
  return $response;
}

/**
 * Passbeemedia Web Push login request.
 */
function passbeemedia_login($passbeemedia_user, $passbeemedia_pass) {
  $remote_content = array(
    'username' => $passbeemedia_user,
    'password' => $passbeemedia_pass,
  );

  $remote_data = array(
    'method' => 'POST',
    'remoteAction' => 'login',
    'appkey' => $passbeemedia_user,
    'appsecret' => $passbeemedia_pass,
    'remoteContent' => json_encode($remote_content),
  );
  return passbeemedia_remote_request($remote_data);
}

/**
 * Passbeemedia Web Push app stats request.
 */
function passbeemedia_get_stats($passbeemedia_app_key, $passbeemedia_app_secret) {
  $remote_data = array(
    'method' => 'POST',
    'remoteAction' => 'stats/app',
    'appkey' => $passbeemedia_app_key,
    'appsecret' => $passbeemedia_app_secret,
    'remoteContent' => '',
  );
  return passbeemedia_remote_request($remote_data);
}

/**
 * Passbeemedia Web Push app settings request.
 */
function passbeemedia_get_remote_settings($passbeemedia_app_key, $passbeemedia_app_secret) {
  $remote_data = array(
    'method' => 'POST',
    'remoteAction' => 'app',
    'appkey' => $passbeemedia_app_key,
    'appsecret' => $passbeemedia_app_secret,
    'remoteContent' => '',
  );
  return passbeemedia_remote_request($remote_data);
}

/**
 * Passbeemedia Web Push send notifications.
 */
function passbeemedia_send_notification($alert, $url, $image_url, $passbeemedia_app_key, $passbeemedia_app_secret) {
  $remote_content = array(
    'alert' => $alert,
  );
  if ($url) {
    $remote_content['url'] = $url;
  }
  if ($image_url) {
    $remote_content['imageURL'] = $image_url;
  }
  $remote_data = array(
    'method' => 'POST',
    'remoteAction' => 'push',
    'appkey' => $passbeemedia_app_key,
    'appsecret' => $passbeemedia_app_secret,
    'remoteContent' => json_encode($remote_content),
  );
  return passbeemedia_remote_request($remote_data);
}
