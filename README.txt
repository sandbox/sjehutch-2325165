Passbeemedia Web Push provides direct engagement with your readers by sending push
notifications to their web browser or mobile device.

Push Module Features:

- Safari Web Push - Take advantage of web push (Desktop Push Notifications)
on OS X Mavericks.

- Detailed Analytics - Real-Time stats straight within Drupal.
- Auto Notifications - We take the work out of it. Automatically send
notifications to your readers when creating a new post.
- Scheduled Post Notifications - Not posting now? Don't worry. Your
alerts will go out when your content posts.
- Optional mobile push solution - Fully supported solution to send
push not only to desktop, but mobile devices.
- Free Passbeemedia Web Push account included. No setup fees, no surprises, and no
limitations on your site visitors.

Using Features

Safari Web Push To use Safari Web Push, simply activate the Passbeemedia Web Push
for Drupal module. When your site is viewed in a push-enabled browser,
the browser will prompt for permission. No additional setup is needed.

Auto Push Auto Push is what makes this module stellar. When you create
a new post, your post's title, link, and featured image (if one is attached)
will be sent to all of your subscribed readers. That's it. Really. You don't
have to do anything else. (It works just like magic.) When enabled, an auto
push override check box is also placed on your post's admin page. If for
some reason you do not want a notification to go out for that post, just
check the box when publishing.

Send Manual Alert You can send a manual alert to all subscribed users by
entering your message text in the "Notification Text" box and a link in
the "Notification Link" box.

Activate all Passbeemedia Web Push Features When checked, your site will automatically
receive the newest (and coolest) features from Passbeemedia Web Push as we roll them
out. (Like Chrome Web Push. cough)

Passbeemedia Web Push Analytics & Passbeemedia Web Push JS Detailed metrics are provided in the dashboard.
These metrics include subscribers, notifications sent, average time on
site, and total page views.

Mobile Push Support To enable mobile push support, check the box.
The Passbeemedia Web Push Header bar, will be inserted onto your page.

FAQ
What does Passbeemedia Web Push cost?
It's free! No setup fees, no surprises, no limitations on your site visitors or
subscribers.

Free? Really? What's the catch?
No catch! We believe in providing a great service and making it accessible to
everyone. So... It's free.
(Up to 1,000,000 messages. If you hit that... We'll need to talk.)

Is it really this easy to use Safari Web Push on my site?
Yup! Cool right?

Do I need and Apple Developer Account or Google Play Developer account?
Nope. We've got you covered.

Do you support Google Chrome or Firefox for desktop push?
Almost. We will be releasing support for Google Chrome very soon. Firefox will
follow. To take advantage of them as we include support, make sure to have
Activate all Passbeemedia Web Push Features checked.

If I'm using Mobile Push Support, Why do subscribers have to download the
Passbeemedia Web Push mobile app?
In order for a device to receive a true push notification, a native mobile
app must be installed. That's where Passbeemedia Web Push comes in. We are there to provide
that native bridge. We do not pull readers into our app, but push them back
to your website. Notifications also stay in the Passbeemedia Web Push app, which makes them
able to be viewed at any time.

Do my readers / subscribers have to create an account with Passbeemedia Web Push?
Nope. We have a patent-pending zero-configuration installation process for
the Passbeemedia Web Push app. When a person hits your subscription link, the are sent to
the Passbeemedia Web Push page on their device's app store, and prompted for install. Once
installed and opened, a person is sent directly back to the page they
were viewing on your site.
