<?php

/**
 * @file
 * Passbeemedia Web Push module admin page with Passbeemedia Web Push account stats.
 */

/**
 * Checks to see if Passbeemedia Web Push is logged in.
 */
function passbeemedia_admin_page() {
  $key = variable_get('passbeemedia_key', '');
  $secret = variable_get('passbeemedia_secret', '');
  if ($key && $secret) {
    return drupal_get_form('passbeemedia_plugin_form');
  }
  else {
    return drupal_get_form('passbeemedia_admin_form');
  }
}

/**
 * Implements hook_form().
 */
function passbeemedia_admin_form($node, &$form_state) {
  global $base_url;
  $favicon = $base_url . base_path() . drupal_get_path('module', 'passbeemedia') . '/images/passbeemedia_thumb.png';
  $type = theme_get_setting('favicon_mimetype');
  drupal_add_html_head_link(
    array(
      'rel' => 'shortcut icon',
      'href' => $favicon,
      'type' => $type)
    );

  module_load_include('inc', 'passbeemedia', 'passbeemedia.api');
  $values = isset($form_state['values']) ? $form_state['values'] : array('passbeemedia_user' => '');

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'passbeemedia') . '/css/passbeemediastyle.css',
  );

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'passbeemedia') . '/js/passbeemedia.admin.js',
  );

  $form['login'] = array(
    '#prefix' => '<div class="passbeemediaer_box"><div id="passbeemediaLoginForm">',
    '#suffix' => '</div></div>',
  );

  $stage = isset($form_state['stage']) ? $form_state['stage'] : 1;

  if ($stage == 1) {
    $form['login']['passbeemedia_header'] = array(
      '#markup' => t('<div id="passbeemediaer"><div id="logo"></div><div class="passbeemediaer_text">Welcome! Login into your Passbeemedia Web Push account below.<br />If you don\'t have an account,') . ' ' . l(t('sign up for free.'), 'http://webpush.passbeemedia.com/?source=drupal&returnURL=' . $base_url . '/admin/config/administration/passbeemedia', array('attributes' => array('target' => '_blank'))) . '</div></div>',
    );
    $form['login']['passbeemedia_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Username / Email'),
      '#default_value' => $values['passbeemedia_user'],
      '#size' => 35,
      '#maxlength' => 50,
      '#required' => TRUE,
      '#attributes' => array('placeholder' => t('Enter your Email')),
    );
    $form['login']['passbeemedia_pass'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#size' => 35,
      '#maxlength' => 50,
      '#required' => TRUE,
      '#attributes' => array('placeholder' => t('Enter your Password')),
    );
    /*
    $form['login']['forgot'] = array(
      '#markup' => l(t('forgot password?'), 'http://webpush.passbeemedia.com/', array('attributes' => array('target' => '_blank', 'id' => 'passbeemediaForgotPassword'))),
    );
    */

    $form['login']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Login'),
    );
  }
  elseif ($stage == 2) {
    $form['login']['passbeemedia_header'] = array(
      '#markup' => '<div id="passbeemediaer"><div id="logo"></div><div class="passbeemediaer_text"><br />Select which site do you want to access, <br />or ' . l(t('sign in into another account.'), 'admin/config/administration/passbeemedia') . '</div></div>',
    );
    foreach ($form_state['apps'] as $key => $site) {
      $sites[$key] = $site->name;
      $web[$key] = $site->name . ':' . $site->key . ':' . $site->secret;
    }
    $form['login']['site'] = array(
      '#type' => 'select',
      '#title' => t('Choose Your Site'),
      '#options' => $sites,
      '#description' => t('To switch sites after you log in, you will need to log out.'),
    );

    $form['login']['web'] = array(
      '#value' => $web,
      '#type' => 'value',
    );

    $form['login']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Continue'),
    );
  }
  return $form;
}

/**
 * Implements hook_submit().
 */
function passbeemedia_admin_form_submit($form, &$form_state) {
  module_load_include('inc', 'passbeemedia', 'passbeemedia.api');
  $values = $form_state['values'];

  if (!isset($form_state['apps'])) {
    $login_into_passbeemedia = passbeemedia_login($values['passbeemedia_user'], $values['passbeemedia_pass']);
    $data = json_decode($login_into_passbeemedia->data);
    $site = 0;
  }
  else {
    $site = $values['site'];
    $web = $values['web'];
    $website = explode(':', $web[$site]);
    $data->apps[] = $form_state['apps'][$site];
    $data->success = TRUE;
  }

  if ($data->success === 1) {
    if (count($data->apps) == 1) {
      if (empty($website)) {
        variable_set('passbeemedia_key', $data->apps[$site]->key);
        variable_set('passbeemedia_name', $data->apps[$site]->name);
        variable_set('passbeemedia_secret', $data->apps[$site]->secret);
      }
      else {
        variable_set('passbeemedia_name', $website['0']);
        variable_set('passbeemedia_key', $website['1']);
        variable_set('passbeemedia_secret', $website['2']);
      }
      $status = t('<b>Welcome to Passbeemedia Web Push!</b> The plugin is up and running and visitors to your site using Safari on OS X Mavericks are currently being prompted to subscribe for push notifications. Once you have subscribers you\'ll be able see recent activity, all-time stats, and send manual push notifications. If you have questions or need support, just email us at <a href="mailto:support@passbeemedia.com" target="_blank">support@passbeemedia.com</a>.');
      drupal_set_message($status);
    }
    else {
      $form_state['rebuild'] = TRUE;
      $form_state['apps'] = $data->apps;
      $form_state['stage'] = 2;
    }
  }
  else {
    drupal_set_message(t('Please check your Email or Username and Password.'));
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Implements hook_form().
 */
function passbeemedia_plugin_form() {
  global $base_url;
  $favicon = $base_url . base_path() . drupal_get_path('module', 'passbeemedia') . '/images/passbeemedia_thumb.png';
  $path = drupal_get_path('module', 'passbeemedia');
  $type = theme_get_setting('favicon_mimetype');
  drupal_add_html_head_link(
    array(
      'rel' => 'shortcut icon',
      'href' => $favicon,
      'type' => $type,
    )
  );
  drupal_add_css($path . '/css/passbeemediastyle.css');

  module_load_include('inc', 'passbeemedia', 'passbeemedia.api');

  $key = variable_get('passbeemedia_key', '');
  $secret = variable_get('passbeemedia_secret', '');

  $passbeemedia_stats = passbeemedia_get_stats($key, $secret);

  if (isset($passbeemedia_stats->data)) {
    $data = json_decode($passbeemedia_stats->data);
  }

  $form['user'] = array(
    '#prefix' => '<div id="passbeemedia_top_right">',
    '#markup' => '<div id="passbeemedia_user">' . check_plain(variable_get('passbeemedia_name', '')) . '</div>',
  );
  $form['signout'] = array(
    '#markup' => l(t('Sign out'), 'admin/config/administration/passbeemedia/signout'),
    '#suffix' => '</div>',
  );

  $form['recent'] = array(
    '#prefix' => '<div class="" id="passbeemediaer_recent_activity">',
    '#title' => '',
    '#suffix' => '</div>',
  );

  $form['all-time'] = array(
    '#prefix' => '<div class="" id="passbeemediaer_all_time">',
    '#type' => 'fieldset',
    '#title' => t('All-time stats'),
    '#suffix' => '</div>',
  );
  $form['all-time']['total'] = array(
    '#markup' => '<div><span class="passbeemedia-stat">' . number_format($data->registrations) . '</span>Total subscribers</div>',
  );
  $form['all-time']['notifications-sent'] = array(
    '#markup' => '<div><span class="passbeemedia-stat">' . number_format($data->notifications) . '</span>Messages sent</div>',
  );
  $form['all-time']['notifications-deliveries'] = array(
    '#markup' => '<div><span class="passbeemedia-stat">' . number_format($data->messages) . '</span>Notification deliveries</div>',
  );
  /*
  $form['all-time']['avg-on-site'] = array(
    '#markup' => '<div><span class="passbeemedia-stat">' . number_format($data->timeOnSite / 60000) . '<span class="passbeemedia-stat-tag"> mins</span></span>Time on site</div>',
  );
  */
  $form['all-time']['pageviews'] = array(
    '#markup' => '<div><span class="passbeemedia-stat">' . number_format($data->pageViewCount) . '</span>Total page views</div>',
  );

  $form['manual-push'] = array(
    '#prefix' => '<div class="" id="passbeemediaer_manual_push">',
    '#type' => 'fieldset',
    '#title' => t('Send a manual push notification'),
    '#suffix' => '</div>',
  );
  $form['manual-push']['manualtext'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#attributes' => array('placeholder' => t('Enter message')),
    '#description' => t('Enter the text for the message you would like to send your subscribers.'),
  );
  $form['manual-push']['manuallink'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#attributes' => array('placeholder' => t('Enter message URL (http://mysite.com)')),
    '#description' => t('Enter a website link (URL) that your subscribers will be sent to upon clicking the message.'),
  );
  $form['manual-push']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send notification'),
    '#validate' => array('passbeemedia_manual_push_notification_validate'),
    '#submit' => array('passbeemedia_manual_push_notification_submit'),
  );

  $passbeemedia_remote_settings = passbeemedia_get_remote_settings($key, $secret);

  if (!empty($passbeemedia_remote_settings->data)) {
    $passbeemedia_remote_settings->data = json_decode($passbeemedia_remote_settings->data);
    variable_set('passbeemedia_mobile_push_support', $passbeemedia_remote_settings->data->passbeemediaBarSetting);
    variable_set('passbeemedia_all_features', $passbeemedia_remote_settings->data->autoUpdate);
  }

  $form['settings'] = array(
    '#prefix' => '<div class="" id="passbeemediaer_settings">',
    '#type' => 'fieldset',
    '#title' => t('Module Settings'),
    '#suffix' => '</div>',
  );
  $form['settings']['passbeemedia_auto_push'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto push'),
    '#default_value' => variable_get('passbeemedia_auto_push', FALSE),
    '#description' => t('Enabling this will automatically send a push notification to your subscribers every time you publish a new article or blog entry'),
  );
  $form['settings']['passbeemedia_mobile_push_support'] = array(
    '#type' => 'checkbox',
    '#title' => t('Mobile push support'),
    '#default_value' => variable_get('passbeemedia_mobile_push_support') === "TOP" ? TRUE : FALSE,
    '#description' => t('Enabling this will allow your readers to subscribe and receive push notifications on their phone or tablet when they view your mobile site. First-time subscribers will be prompted to install the iOS or Android Passbeemedia Web Push app in order to receive notifications.'),
  );
  $form['settings']['passbeemedia_all_features'] = array(
    '#title' => t('Activate all passbeemedia features'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('passbeemedia_all_features'),
    '#description' => t('Enabling this will automatically activate current and future features as they are added.'),
  );
  $form['settings']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('passbeemedia_plugin_form_submit'),
  );
  return $form;
}

/**
 * Implements hook_submit().
 */
function passbeemedia_plugin_form_submit($form, $form_state) {
  $passbeemedia_app_key = variable_get('passbeemedia_key');
  $passbeemedia_app_secret = variable_get('passbeemedia_secret');

  $values = &$form_state['values'];
  variable_set('passbeemedia_auto_push', $values['passbeemedia_auto_push']);
  variable_set('passbeemedia_mobile_push_support', $values['passbeemedia_mobile_push_support']);
  variable_set('passbeemedia_all_features', $values['passbeemedia_all_features']);

  $remote_content = array(
    'passbeemediaBarSetting' => $values['passbeemedia_mobile_push_support'] ? 'TOP' : 'OFF',
    'autoUpdate' => $values['passbeemedia_all_features'] ? TRUE : FALSE,
  );

  $remote_data = array(
    'method' => 'PUT',
    'remoteAction' => 'app',
    'appkey' => $passbeemedia_app_key,
    'appsecret' => $passbeemedia_app_secret,
    'remoteContent' => json_encode($remote_content),
  );
  passbeemedia_remote_request($remote_data);
}

/**
 * Implements hook_validate().
 */
function passbeemedia_manual_push_notification_validate($form, $form_state) {
  if (!$form_state['values']['manualtext']) {
    form_set_error('manualtext', t('Your message can not be blank.'));
  }
  if (!$form_state['values']['manuallink']) {
    form_set_error('manuallink', t('Your link can not be blank.'));
  }
  else {
    $manual_link = $form_state['values']['manuallink'];
    if (strpos($manual_link, 'http') === FALSE) {
      $manual_link = 'http://' . $manual_link;
    }
    if (!valid_url($manual_link, TRUE)) {
      form_set_error('manuallink', t('Your link is invalid.'));
    }
  }
}

/**
 * Implements hook_submit().
 */
function passbeemedia_manual_push_notification_submit($form, $form_state) {
  $values = $form_state['values'];
  $manual_text = $values['manualtext'];
  $manual_link = $values['manuallink'];
  $passbeemedia_app_key = variable_get('passbeemedia_key', '');
  $passbeemedia_app_secret = variable_get('passbeemedia_secret', '');
  if ($manual_link && strpos($manual_link, 'http') === FALSE) {
    $manual_link = 'http://' . $manual_link;
  }
  $msg_status = passbeemedia_send_notification($manual_text, $manual_link, FALSE, $passbeemedia_app_key, $passbeemedia_app_secret);

  $msg_status->data = json_decode($msg_status->data);
  if ($msg_status->data->success === TRUE) {
    $status = t('Message Sent.');
  }
  else {
    $status = $msg_status->data->error;
  }
  drupal_set_message(check_plain($status));
}

/**
 * Removes account specific database values used by Passbeemedia Web Push module on Sign Out.
 */
function passbeemedia_admin_signout() {
  variable_del('passbeemedia_key');
  variable_del('passbeemedia_secret');
  variable_del('passbeemedia_name');
  variable_del('passbeemedia_all_features');
  variable_del('passbeemedia_mobile_push_support');
  drupal_goto('admin/config/administration/passbeemedia');
}
