/**
 * @file
 * JavaScript for the Passbeemedia Web Push for Drupal module.
 *
 * JavaScript that aids in the visual appearance of the Passbeemedia web push admin
 * section within the Drupal admin.
 */

(function($) {
  Drupal.behaviors.roost = {
    attach: function (context, settings) {
      if ($('#passbeemediaLoginForm').length) {
        $('#branding').css('background-image', 'none');
      }
    }
  }
})(jQuery);
