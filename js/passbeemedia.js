/**
 * @file
 * JavaScript for the Passbeemedia Web Push for Drupal module.
 *
 * Script that includes the required Passbeemedia Web Push JS file onto the site.
 */

!function(d,s,id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if(!d.getElementById(id)){
    js = d.createElement(s); js.id = id;
    js.src = '//webpush.passbeemedia.com/assets/js/pbm_push.js';
    fjs.parentNode.insertBefore(js,fjs);
  }
}(document, 'script', 'passbeemedia-js');
